import std.stdio;
import std.random;

char[80] chip8_fontset =[
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];



class Emulator
{
	ushort opcode; // current opcode
	char[4096] memory; // 4K memory
	char[16] V; // general purpose registers
	ushort index;
	ushort pc;
	public char[64 * 32] screen; //64 x 32 Screen size
	char delayTimer;
	char soundTimer;

	// 16 Level stack
	ushort[16] stack;
	ushort sp;

	public bool drawFlag = false;

	public char[16] keys; // Keypad, hex based (0x0-0xF)

	this(char[] program)
	{
		index=0;
		pc = 0x200; // Program starts at 0x200
		for (int i = 0; i < 80; ++i)
			memory[i] = chip8_fontset[i];
		for (int i = 0; i < program.length; ++i)
			memory[512 + i] = program[i];
		for (int i = 0; i < keys.length; ++i)
			keys[i] = cast(char) 0;
		cls();
	}

	private void cls()
	{
		for (int i = 0; i < screen.length; ++i)
			screen[i] = cast(char) 0;
	}

	public void cycle()
	{
		debug{writefln("Program Counter at: %x",pc);}
		opcode = memory[pc] << 8 | memory[pc + 1];
		debug{writefln("Fetched opcode %x",opcode);}
		switch (opcode & 0xF000)
		{
			case 0x0000:
				if ((opcode & 0x000F) == 0xE)
				{
					sp--;
					pc = stack[sp];
				}
				else if ((opcode & 0x000F) == 0x0)
				{
					cls();
				}
				pc +=2;
				break;
			case 0xA000: // ANNN: Sets I to the address NNN
				index = opcode & 0x0FFF;
				pc += 2;
				break;
			case 0x1000:
				pc = opcode & 0x0FFF;
				break;
			case 0x2000: // 2NNN: Calls subroutine at NNN
				stack[sp++] = pc;
				pc = opcode & 0x0FFF;
				break;
			case 0x3000:
				auto location = (opcode & 0x0F00) >> 8;
				if (V[location] == (opcode & 0x00FF))
					pc += 2;
				pc += 2;
				break;

			case 0x4000:
				auto location = (opcode & 0x0F00) >> 8;
				if (V[location] != (opcode & 0x00FF))
					pc += 2;
				pc += 2;
				break;

			case 0x5000:
				ushort x = V[(opcode & 0x0F00) >> 8];
				ushort y = V[(opcode & 0x00F0) >> 4];
				if (V[x] == V[y])
					pc += 2;
				pc += 2;
				break;
	
			case 0x6000: // 6xkk: put value kk in register V[x]
				auto location = (opcode & 0x0F00) >> 8; // Bitshift to get 0x0N00 to 0xN
				V[location] = (opcode & 0x00FF);
				debug{writefln("Writing %x into V[%x]",opcode & 0x00FF, location);}
				pc += 2;
				break;
			case 0xD000: // Draw
				ushort x = V[(opcode & 0x0F00) >> 8];
				ushort y = V[(opcode & 0x00F0) >> 4];
				ushort height = opcode & 0x000F;
				ushort pixel;

				V[0xF] = 0;
				for (int yline = 0; yline < height; yline++)
				{
					pixel = memory[index + yline];
					for(int xline = 0; xline < 8; xline++)
					{
						if((pixel & (0x80 >> xline)) != 0)
							{
							if(screen[(x + xline + ((y + yline) * 64))] == 1)
								V[0xF] = 1;
							screen[x + xline + ((y + yline) * 64)] ^= 1;
							}
					}
				}

				drawFlag = true;
				pc += 2;
				break;

			case 0x7000:
				auto location = (opcode & 0x0F00) >> 8; // Bitshift to get 0x0N00 to 0xN
				V[location] = cast(char) (V[location] + (opcode & 0x00FF));
				debug{writefln("Adding %x to V[%x]",opcode & 0x00FF, location);}
				pc += 2;
				break;

			case 0x8000:
				switch (opcode & 0x000F)
				{
					case 0x0000: // 0x8XY0: Sets VX to the value of VY
						V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0001: // 0x8XY1: Sets VX to "VX OR VY"
						V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0002: // 0x8XY2: Sets VX to "VX AND VY"
						V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0003: // 0x8XY3: Sets VX to "VX XOR VY"
						V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0004: // 0x8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't
						if(V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8]))
							V[0xF] = 1; //carry
						else
							V[0xF] = 0;
						V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0005: // 0x8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
						if(V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
							V[0xF] = 0; // there is a borrow
						else
							V[0xF] = 1;
						V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0006: // 0x8XY6: Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift
						V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x1;
						V[(opcode & 0x0F00) >> 8] >>= 1;
						pc += 2;
					break;

					case 0x0007: // 0x8XY7: Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't
						if(V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])	// VY-VX
							V[0xF] = 0; // there is a borrow
						else
							V[0xF] = 1;
						V[(opcode & 0x0F00) >> 8] = cast(char) (V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8]);
						pc += 2;
					break;

					case 0x000E: // 0x8XYE: Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift
						V[0xF] = V[(opcode & 0x0F00) >> 8] >> 7;
						V[(opcode & 0x0F00) >> 8] <<= 1;
						pc += 2;
					break;

					default:
						writefln("Unknown opcode: %x",opcode);
						assert(false,"Unknown opcode");
				}
				break;
			case 0x9000:
				auto addr1 = (opcode & 0x0F00) >> 8;
				auto addr2 = (opcode & 0x00F0) >> 4;
				debug{writefln("Checking with current pc: %x",pc);}
				if (V[addr1] != V[addr2])
					pc += 2;
				pc += 2;
				debug{writefln("set pc: %x",pc);}
				break;
			case 0xC000:
				auto location = (opcode & 0x0F00) >> 8;
				auto randnum = std.random.uniform(0,256) & (opcode & 0x00FF);
				V[location] = cast(char) randnum;
				pc += 2;
				break;
			case 0xE000:
				switch(opcode & 0x00FF)
				{
					case 0x009E: // EX9E: Skips the next instruction if the key stored in VX is pressed
						if(keys[V[(opcode & 0x0F00) >> 8]] != 0)
							pc += 4;
						else
							pc += 2;
					break;

					case 0x00A1: // EXA1: Skips the next instruction if the key stored in VX isn't pressed
						if(keys[V[(opcode & 0x0F00) >> 8]] == 0)
							pc += 4;
						else
							pc += 2;
					break;

					default:
						writefln("Unknown opcode: %x",opcode);
						assert(false,"Unknown opcode");
				}
				break;
			case 0xF000:
				switch (opcode & 0x00FF)
				{
					case 0x0033:
						memory[index] = V[(opcode & 0x0F00) >> 8] / 100;
						memory[index + 1] = (V[(opcode & 0x0F00) >> 8] /10) % 100;
						memory[index + 2] = (V[(opcode & 0x0F00) >> 8] % 100) % 10;
						pc += 2;
						break;
					case 0x001E:
						auto location = (opcode & 0x0F00) >> 8;
						index += V[location];
						pc += 2;
						break;

					case 0x0007: // FX07: Sets VX to the value of the delay timer
						V[(opcode & 0x0F00) >> 8] = delayTimer;
						pc += 2;
					break;

					case 0x000A: // FX0A: A key press is awaited, and then stored in VX
					{
						bool keyPress = false;

						for(int i = 0; i < 16; ++i)
						{
							if(keys[i] != 0)
							{
								V[(opcode & 0x0F00) >> 8] = cast(char) i;
								keyPress = true;
							}
						}

						// indexf we didn't received a keypress, skip this cycle and try again.
						if(!keyPress)
							return;

						pc += 2;
					}
					break;

					case 0x0015: // FX15: Sets the delay timer to VX
						delayTimer = V[(opcode & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x0018: // FX18: Sets the sound timer to VX
						soundTimer = V[(opcode & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x0029: // FX29: Sets index to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font
						index = V[(opcode & 0x0F00) >> 8] * 0x5;
						pc += 2;
					break;

					case 0x0055: // FX55: Stores V0 to VX in memory starting at address index
						for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
							memory[index + i] = V[i];

						// On the original interpreter, when the operation is done, index = index + X + 1.
						index += ((opcode & 0x0F00) >> 8) + 1;
						pc += 2;
					break;

					case 0x0065: // FX65: Fills V0 to VX with values from memory starting at address index
						for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
							V[i] = memory[index + i];

						// On the original interpreter, when the operation is done, index = index + X + 1.
						index += ((opcode & 0x0F00) >> 8) + 1;
						pc += 2;
					break;

					default:
						writefln("Unknown opcode: %x",opcode);
						assert(false,"Unknown opcode");
				}
				break;
			default:
				writefln("Unknown opcode: %x",opcode);
				assert(false,"Unknown opcode");
		}
		if (delayTimer > 0)
			--delayTimer;
		if (soundTimer > 0)
		{
			if (soundTimer == 1)
				writeln("BEEP");
			--soundTimer;
		}
	}

	public void printDebug()
	{
		writefln("Programm Counter at: %x",pc);
		writeln("Stack: ");
		writeln(stack);
	}
}
