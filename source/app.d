import std.stdio;
import std.string;
import core.thread;
import std.conv;
import derelict.sdl2.sdl;
import gfm.sdl2.sdl;
import gfm.sdl2.keyboard;
import gfm.sdl2.mouse;
import gfm.sdl2.window;
import gfm.sdl2.renderer;
import std.experimental.logger;
import emulator;

int scale = 10;

void main(string[] args)
{
	auto fstr = args[2];
	auto del = to!int(args[1]);
	auto logger = new std.experimental.logger.TestLogger();
	auto sdl = new SDL2(logger);
	auto window = new SDL2Window(sdl,0,0,64*scale,32*scale,SDL_WINDOW_INPUT_FOCUS);
	auto renderer = new SDL2Renderer(window);
	renderer.setColor(0,0,0,255);
	renderer.clear();

	auto file = File(fstr,"rb");
	char[2048] buffer;
	char[] readBuff = file.rawRead(buffer);
	auto emu = new Emulator(readBuff);

	try 
	{
		while (!sdl.keyboard.isPressed(27))
		{
			SDL_Event ev;
			sdl.pollEvent(&ev);
			
			char[16] keys;
			// 1 2 3 C
			// 4 5 6 D
			// 7 8 9 E
			// A 0 B F

			keys[1] = sdl.keyboard.isPressed(SDLK_1);
			keys[2] = sdl.keyboard.isPressed(SDLK_2);
			keys[3] = sdl.keyboard.isPressed(SDLK_3);
			keys[0xC] = sdl.keyboard.isPressed(SDLK_4);

			keys[4] = sdl.keyboard.isPressed(SDLK_q);
			keys[5] = sdl.keyboard.isPressed(SDLK_w);
			keys[6] = sdl.keyboard.isPressed(SDLK_e);
			keys[0xD] = sdl.keyboard.isPressed(SDLK_r);

			keys[7] = sdl.keyboard.isPressed(SDLK_a);
			keys[8] = sdl.keyboard.isPressed(SDLK_s);
			keys[9] = sdl.keyboard.isPressed(SDLK_d);
			keys[0xE] = sdl.keyboard.isPressed(SDLK_f);

			keys[0xA] = sdl.keyboard.isPressed(SDLK_y);
			keys[0] = sdl.keyboard.isPressed(SDLK_x);
			keys[0xB] = sdl.keyboard.isPressed(SDLK_c);
			keys[0xF] = sdl.keyboard.isPressed(SDLK_v);

			emu.keys = keys;
			emu.cycle();
			if (emu.drawFlag)
			{
				drawScreen(emu.screen,&renderer);
				emu.drawFlag = false;
			}
			Thread.sleep(dur!("msecs")(del/10));
		}
	}
	catch (Exception ex)
	{
		writeln(ex);
	}
	finally
	{
		renderer.destroy();
		window.destroy();
		sdl.destroy();
	}
}

void drawScreen(char[] screen,SDL2Renderer* renderer)
{
	renderer.setColor(19,19,19,255);
	renderer.clear();

	renderer.setColor(238,238,238,255);
	for (int i = 0; i < 32; i++)
	{
		for(int j = 0; j < 64; j++)
		{
			auto line = format("%b",screen[i*64+j]);
			foreach (c;line)
			{
				if (c == '1')
					renderer.fillRect(j*scale,i*scale,scale,scale);
			}
		}
	}
	renderer.present();
}
