# chip8

# Building

```{.bash}
dub build -b release
```

# Running

```{.bash}
./chip8 25 source.c8
# 25 is the delay time/clock rate
```
